using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System;
using System.Net;
using System.Net.Sockets;




public class SendImage : MonoBehaviour
{
    private PIXEL_FORMAT mPixelFormat = PIXEL_FORMAT.UNKNOWN_FORMAT;
    private bool mFormatRegistered = false;
    private TcpClient client;
    private NetworkStream dataStream;

    private Sprite img1;
    public GameObject MyImage;

    // This is the server's IP address.  If server is on Windows, find IP address using the "ipconfig"
    // command in a terminal window.  If both server and client are on local machine, use 127.0.0.1.
 
    public string serverIP = "192.168.1.43";   // "127.0.0.1";
    public Int32 port = 9500;

    // Get a stream object for reading and writing.

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_EDITOR
        mPixelFormat = PIXEL_FORMAT.GRAYSCALE; // Need Grayscale for Editor
#else
      mPixelFormat = PIXEL_FORMAT.RGB888; // Use RGB888 for mobile
#endif

        // Register Vuforia life-cycle callbacks:
        VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        //VuforiaARController.Instance.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
        VuforiaARController.Instance.RegisterOnPauseCallback(OnPause);

        Debug.Log("Trying to connect to server ...");
        try
        {
            client = new TcpClient(serverIP, port);

            // Get a stream object for reading and writing.
            dataStream = client.GetStream();
            Debug.Log(".. connected!");
            
        }
        catch (Exception e)
        {
            Debug.Log("Can't connect to server!\n");
            Debug.Log(e);
        }
    }

    // This is called when the app shuts do                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             wn or switches to another Scene.
    void OnDestroy()
    {
        Debug.Log("Closing the socket ...");
        dataStream.Close();
        client.Close();
    }
    private void OnVuforiaStarted()
    {
        // Try register camera image format
        if (CameraDevice.Instance.SetFrameFormat(mPixelFormat, true))
        {
            Debug.Log("Successfully registered pixel format " + mPixelFormat.ToString());
            mFormatRegistered = true;
        }
        else
        {
            Debug.LogError(
                "\nFailed to register pixel format: " + mPixelFormat.ToString() +
                "\nThe format may be unsupported by your device." +
                "\nConsider using a different pixel format.\n");
            mFormatRegistered = false;
        }
        // Note - even though camera image is not null at this point, it seems to have zero size.
        // It may take a couple of frames to be initialized properly.
    }


    // Called when app is paused / resumed
    void OnPause(bool paused)
    {
        if (paused)
        {
            Debug.Log("App was paused");
            UnregisterFormat();
        }
        else
        {
            Debug.Log("App was resumed");
            RegisterFormat();
        }
    }

    // Register the camera pixel format
    void RegisterFormat()
    {
        if (CameraDevice.Instance.SetFrameFormat(mPixelFormat, true))
        {
            Debug.Log("Successfully registered camera pixel format " + mPixelFormat.ToString());
            mFormatRegistered = true;
        }
        else
        {
            Debug.LogError("Failed to register camera pixel format " + mPixelFormat.ToString());
            mFormatRegistered = false;
        }
    }

    // Unregister the camera pixel format (e.g. call this when app is paused)
    void UnregisterFormat()
    {
        Debug.Log("Unregistering camera pixel format " + mPixelFormat.ToString());
        CameraDevice.Instance.SetFrameFormat(mPixelFormat, false);
        mFormatRegistered = false;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("its is running the send image server function");
        SendImageToServer();
    }

    // This will be called when the "Send Image" button is pressed.
    public void SendImageToServer()
    {
        Debug.Log("Sending image to server ...");
        if (!mFormatRegistered)
        {
            Debug.Log("Format not registered");
            return;
        }
        
        Vuforia.Image image = CameraDevice.Instance.GetCameraImage(mPixelFormat);
        //Vuforia.Image image = CameraDevice.Instance.GetCameraImage(PIXEL_FORMAT.UNKNOWN_FORMAT);
        // Check that the camera image is not null, since it can take a few frames 
        // for the image to become available after registering for an image format.
        if (image == null)
        {
            Debug.Log("No image output");
            return;
        }

        // Optionally print some camera parameters to the console.
        Vector2 fov = CameraDevice.Instance.GetCameraFieldOfViewRads();
       
        Debug.Log(fov[0]);      // field of view
        Debug.Log(fov[1]);
        Debug.Log(
            "\nImage Format: " + image.PixelFormat +
            "\nImage Size:   " + image.Width + "x" + image.Height +
            "\nBuffer Size:  " + image.BufferWidth + "x" + image.BufferHeight +
            "\nImage Stride: " + image.Stride + "\n"
        );
        
        // Get the image data.
        byte[] pixels = image.Pixels;
        if (pixels == null || pixels.Length == 0)
            return;

        Debug.Log(
            "First couple of image pixels: " +
            pixels[0] + ", " +
            pixels[1] + ", " +
            pixels[2] + ", ...\n"
        );

        // Send the message to the server.
        dataStream.Write(pixels, 0, pixels.Length);

        // Wait for a response.
        var byteBuffer = new byte[1024];
        int numBytes = dataStream.Read(byteBuffer, 0, byteBuffer.Length);
        var receivedData = System.Text.Encoding.ASCII.GetString(byteBuffer, 0, numBytes);
        Debug.Log($"Received {numBytes} bytes of data from server: {receivedData}");
        //MyImage.AddComponent(typeof(Image));
        //img1 = Resources.Load<Sprite>(receivedData);
        //MyImage.GetComponent<Image>().sprite = img1;
        //Debug.Log("Received");

        


    }
}

