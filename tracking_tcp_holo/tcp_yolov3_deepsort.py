import os
import cv2
import time
import argparse
import torch
import warnings
import numpy as np
import socket
import pickle
import struct

from detector import build_detector
from deep_sort import build_tracker
from utils.draw import draw_boxes
from utils.parser import get_config


TCP_IP = '192.168.1.199'  # '127.0.0.1'
TCP_PORT = 9500
TCP_PORT2 = 8080
IMAGE_WIDTH = 1280
IMAGE_HEIGHT = 720
BUFFER_SIZE = IMAGE_WIDTH * IMAGE_HEIGHT*3
encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]

class VideoTracker(object):
    def __init__(self, cfg, args):
        self.cfg = cfg
        self.args = args
        use_cuda = args.use_cuda and torch.cuda.is_available()
        if not use_cuda:
            warnings.warn("Running in cpu mode which maybe very slow!", UserWarning)

        if args.display:
            cv2.namedWindow("test", cv2.WINDOW_NORMAL)
            cv2.resizeWindow("test", args.display_width, args.display_height)

        if args.cam != -1:
            #print("Using webcam " + str(args.cam))
            #self.vdo = cv2.VideoCapture(args.cam)
            print("using tcp communication")
        else:
            self.vdo = cv2.VideoCapture()
        self.detector = build_detector(cfg, use_cuda=use_cuda)
        self.deepsort = build_tracker(cfg, use_cuda=use_cuda)
        self.class_names = self.detector.class_names


    def __enter__(self):
        if self.args.cam != -1:
            #ret, frame = self.vdo.read()
            #assert ret, "Error: Camera error"
            #self.im_width = frame.shape[0]
            self.im_width = 640
            self.im_height = 480
            #print(self.im_width,self.im_height)


        else:
            assert os.path.isfile(self.args.VIDEO_PATH), "Error: path error"
            self.vdo.open(self.args.VIDEO_PATH)
            self.im_width = int(self.vdo.get(cv2.CAP_PROP_FRAME_WIDTH))
            self.im_height = int(self.vdo.get(cv2.CAP_PROP_FRAME_HEIGHT))
            assert self.vdo.isOpened()

        if self.args.save_path:
            fourcc =  cv2.VideoWriter_fourcc(*'MJPG')
            self.writer = cv2.VideoWriter(self.args.save_path, fourcc, 20, (self.im_width,self.im_height))

        return self


    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            print(exc_type, exc_value, exc_traceback)


    def run(self):
        print('Server program starting ...')
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((TCP_IP, TCP_PORT))
        sock2.bind((TCP_IP, TCP_PORT2))
        sock.listen()
        sock2.listen()

        conn, addr = sock.accept()
        conn2, addr2 = sock2.accept()
        print("Got a connection to")
        print(addr)
        rec_data=bytes()
        data_len=0
        
        idx_frame = 0
        while True:
            idx_frame += 1
            if idx_frame % self.args.frame_interval:
                continue

            start = time.time()
            #_, ori_im = self.vdo.retrieve()
            
            #print(im.shape[0],im.shape[1])
            img_counter=0
            while data_len<(BUFFER_SIZE):
                data = conn.recv(BUFFER_SIZE)
                if not data:
                    break
                #msg_input = data.decode('utf-8')
                #print("Received %d bytes" % len(data))
                rec_data+=data
                data_len+=len(data)
                #print(data_len)
                # Load into numpy array.
                #image_received = np.frombuffer(data, count=BUFFER_SIZE, dtype=np.uint8)
            image_received = np.frombuffer(rec_data,count=BUFFER_SIZE, dtype=np.uint8)
            rec_data=bytes()
            data_len=0
            
            im_large = np.reshape(image_received, (720, 1280,3))
            im1=cv2.resize(im_large,(640,360),interpolation=cv2.INTER_AREA)
            im = cv2.cvtColor(im1, cv2.COLOR_BGR2RGB)
            #cv2.imshow('received im',im)
            # do detection
            bbox_xywh, cls_conf, cls_ids = self.detector(im)
            if bbox_xywh is not None:
                # select person class
                mask = cls_ids==0

                bbox_xywh = bbox_xywh[mask]
                bbox_xywh[:,3:] *= 1.2 # bbox dilation just in case bbox too small
                cls_conf = cls_conf[mask]

                # do tracking
                outputs = self.deepsort.update(bbox_xywh, cls_conf, im)
                print("confidence:",cls_conf)
                # draw boxes for visualization
                if len(outputs) > 0:
                    bbox_xyxy = outputs[:,:4]
                    identities = outputs[:,-1]
                    im = draw_boxes(im, cls_conf, bbox_xyxy, identities)
            out=str(bbox_xyxy)+str(identities)+str(cls_conf)
            end = time.time()
            print("time: {:.03f}s, fps: {:.03f}".format(end-start, 1/(end-start)))
            #print(ori_im.shape[0],ori_im.shape[1])
            if self.args.display:
                cv2.imshow("test", im )
                cv2.waitKey(1)
            rec_data=bytes()
            data_len=0
            #im = np.reshape(ori_im, (480, 640))
            #im = cv2.cvtColor(I, cv2.COLOR_GRAY2RGB)
            #cv2.imshow("Img", im)
            #cv2.waitKey(10)
            #result, frame = cv2.imencode('.jpg', im, encode_param)

            data1 = pickle.dumps(out, 0)
            size = len(data1)
            print("{}: {}".format(img_counter, size))
            
            img_counter += 1

            msg_output = "Got it." + '\r'
            #msg_output = im[1][1:10]
            conn2.sendall(struct.pack(">L", size) + data1)     # echo
            conn.send(msg_output.encode('utf-8'))        
            #msg_output = "Got it" + '\r'
            #conn.send(data)     # echo
            #conn.send(msg_output.encode('utf-8'))
            #if self.args.save_path:
            #    self.writer.write(ori_im)
        conn.close()
        conn2.close()

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("VIDEO_PATH", type=str)
    parser.add_argument("--config_detection", type=str, default="./configs/yolov3.yaml")
    parser.add_argument("--config_deepsort", type=str, default="./configs/deep_sort.yaml")
    parser.add_argument("--ignore_display", dest="display", action="store_false", default=True)
    parser.add_argument("--frame_interval", type=int, default=1)
    parser.add_argument("--display_width", type=int, default=480) #800x600
    parser.add_argument("--display_height", type=int, default=640)
    parser.add_argument("--save_path", type=str, default="./demo/demo.avi")
    parser.add_argument("--cpu", dest="use_cuda", action="store_false", default=True)
    parser.add_argument("--camera", action="store", dest="cam", type=int, default="-1")
    return parser.parse_args()


if __name__=="__main__":
    args = parse_args()
    cfg = get_config()
    cfg.merge_from_file(args.config_detection)
    cfg.merge_from_file(args.config_deepsort)

    with VideoTracker(cfg, args) as vdo_trk:
        vdo_trk.run()
